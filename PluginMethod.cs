﻿using MerthBot.Exceptions;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MerthBot
{
    public class PluginMethod
    {
        public static PluginMethod FromFullMethodName(string fullMethodName)
        {
            _ = fullMethodName.IsNullOrEmpty()
                ? throw new MerthBotException("Method name empty.") : false;

            var splitMethod = fullMethodName.Split('.');
            var methodName = splitMethod.Last();
            var typeName = splitMethod.SkipLast(1).Join(".");

            var type = Type.GetType(typeName, false);
            
            var count = splitMethod.Length - 2;
            while (type == null) 
            {
                var assemblyName = splitMethod.Join(".", count);
                try
                {
                    var asm = Assembly.Load(assemblyName);
                    type = asm.GetType(typeName, false);
                } catch {
                    count -= 1;
                    _ = count == 2
                        ? throw new MerthBotException($"Could not find type for  {fullMethodName}.") : false;
                }
            }

            var methodInfo = type.GetMethod(methodName)
                ?? throw new MerthBotException($"Could not find method {methodName} on type {typeName}.");

            return new PluginMethod(methodInfo);
        }

        private MethodInfo MethodInfo { get; }

        private PluginMethod(MethodInfo methodInfo)
            => MethodInfo = methodInfo;

        public async Task Invoke(params object[] parameters)
        {
            var parameterInfos = MethodInfo.GetParameters();
            var matchedParameters = parameterInfos.SelectArray(pi => parameters.FirstOrDefault(p => pi.ParameterType.IsAssignableFrom(p.GetType())));
            try
            {
                var ret = MethodInfo.Invoke(null, matchedParameters)
                    ?? throw new Exception("Object returned from plugin invocation is null.");
                var task = (ret as Task)
                    ?? throw new Exception("Object returned from plugin invocation not castable to Task.");
                await task;
            } 
            catch (Exception ex)
            {
                throw new MerthBotPluginExecutionException(MethodInfo, $"Error running plugin method {MethodInfo.DeclaringType?.FullName ?? "<no type>"}.{MethodInfo.Name}", ex);
            }
        }
    }
}
