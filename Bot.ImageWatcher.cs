﻿using System.IO;

namespace MerthBot
{
    partial class Bot
    {
        private void ImageWatcher_Renamed(object sender, RenamedEventArgs e)
            => LoadImageDirectory();

        private void ImageWatcher_Handle(object sender, FileSystemEventArgs e)
            => LoadImageDirectory();
    }
}
