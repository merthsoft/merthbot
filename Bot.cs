﻿using Discord;
using Discord.WebSocket;
using MerthBot.Exceptions;
using MerthBot.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace MerthBot
{
    public partial class Bot
    {
        public event EventHandler<LogEvent>? OnLog;

        private DiscordSocketClient DiscordClient { get; set; } = null!;
        private bool FirstStart { get; set; } = true;
        private IConfig Config { get; set; }

        private HttpClient EmojiMapClient { get; } = new HttpClient();
        private readonly Dictionary<string, ImageDirectory> images = new();
        private FileSystemWatcher? ImageWatcher { get; set; }

        private ReadOnlyDictionary<string, ImageDirectory> Images => new(images);
        
        private readonly Dictionary<string, string> EmojiMap = new();
        private string ResolvedFileRoot { get; set; } = null!;

        public Bot(IConfig config)
            => Config = config;

        public Task Initialize()
            => SetConfig(Config);

        public async Task Death()
        {
            await NotifyMaintainer("I have died.");

            Log("Attempting clean up.");
            try
            {
                await DiscordClient.LogoutAsync();
                await DiscordClient.StopAsync();
            }
            catch (Exception ex)
            {
                Log($"Could not log out or stop client.", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }

            try
            {
                DiscordClient?.Dispose();
                EmojiMapClient?.Dispose();
                ImageWatcher?.Dispose();
            }
            catch (Exception ex)
            {
                Log($"Could not dispose objects.", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }

            Log("Quitting.");
        }

        public void Log(string message, LogLevel level = LogLevel.Message)
            => OnLog?.Invoke(this, new LogEvent(message, level));

        public static string? GetEnvironmentVariable(string? variable)
            => variable == null ? null : Environment.GetEnvironmentVariable(variable, EnvironmentVariableTarget.Process)
            ?? Environment.GetEnvironmentVariable(variable, EnvironmentVariableTarget.User)
            ?? Environment.GetEnvironmentVariable(variable, EnvironmentVariableTarget.Machine);

        // TODO: Make this not modify the passed-in config object
        public async Task SetConfig(IConfig configFile)
        {
            Log("Setting config.");

            var token = GetEnvironmentVariable(configFile.TokenEnvironmentVariable)
                        ?? throw new MerthBotException($"Error getting token from environment variable {configFile.TokenEnvironmentVariable}.");

            ResolvedFileRoot = configFile.FileRoot!; // It's ok if this is null because we null check immediately after
            if (string.IsNullOrWhiteSpace(ResolvedFileRoot))
                ResolvedFileRoot = GetEnvironmentVariable(configFile.FileRootEnvironmentVariable)!; // Override null check because we do it below
            
            _ = ResolvedFileRoot ?? throw new MerthBotException("FileRoot is empty, and FileRootEnvironmentVariable isn't set.");

            _ = configFile.Maintainer ?? throw new MerthBotException("Maintainer not configured.");
            if (configFile.Maintainer.ChannelId == 0 || configFile.Maintainer.GuildId == 0 || configFile.Maintainer.UserId == 0)
                throw new MerthBotException($"Maintainer not configured correctly. Channel: {configFile.Maintainer.ChannelId} Guild: {configFile.Maintainer.GuildId} User: {configFile.Maintainer.UserId}");

            if (DiscordClient?.ConnectionState == ConnectionState.Connected)
            {
                await DiscordClient.LogoutAsync();
                await DiscordClient.StopAsync();
                DiscordClient.Dispose();
                DiscordClient = null!;
            }

            DiscordClient = new DiscordSocketClient(new DiscordSocketConfig
            {
                MessageCacheSize = configFile.MessageCacheSize ?? 1000,
            });

            DiscordClient.Connected += DiscordClient_Connected;
            DiscordClient.GuildAvailable += DiscordClient_GuildAvailable;
            DiscordClient.GuildUnavailable += DiscordClient_GuildUnavailable;
            DiscordClient.Disconnected += DiscordClient_Disconnected;
            DiscordClient.MessageReceived += DiscordClient_MessageReceived;
            DiscordClient.MessageDeleted += DiscordClient_MessageDeleted;
            DiscordClient.MessageUpdated += DiscordClient_MessageUpdated;
            DiscordClient.ReactionAdded += DiscordClient_ReactionAdded;
            DiscordClient.ReactionRemoved += DiscordClient_ReactionRemoved;
            DiscordClient.ReactionsCleared += DiscordClient_ReactionsCleared;

            var loadTasks = new List<Task> {
                LoadEmojiMap(),
                Task.Run(LoadImageDirectory),
            };

            Log($"Starting discord.");
            await DiscordClient.LoginAsync(TokenType.Bot, token);
            await DiscordClient.StartAsync();

            await Task.WhenAll(loadTasks);

            ImageWatcher = new FileSystemWatcher(ResolvedFileRoot)
            {
                IncludeSubdirectories = true
            };
            ImageWatcher.Changed += ImageWatcher_Handle;
            ImageWatcher.Deleted += ImageWatcher_Handle;
            ImageWatcher.Renamed += ImageWatcher_Renamed;
            ImageWatcher.EnableRaisingEvents = true;

            Log($"Config loaded.");
        }
        
        private async Task HandleMessage(SocketUserMessage message)
        {
            if (message.Content.IsNullOrEmpty())
                return;

            try
            {
                var (command, splitMessage) = message.FindCommand(Config);

                if (command == null)
                    return;

                var action = command.Action;
                if (string.IsNullOrWhiteSpace(action))
                    await HandleCommand(message, command, splitMessage);
                else
                    await HandleAction(message, command, splitMessage);
            }
            catch (Exception ex)
            {
                Log($"Failed to process message: {message.Content}", LogLevel.Error);
                Log(ex.ToString(), LogLevel.Error);
            }
        }

        private async Task HandleCommand(SocketUserMessage message, ICommand command, string[] splitMessage)
        {
            string? reply = null;
            if (!command.Responses.IsEmpty())
            {
                var args = splitMessage.Skip(1).ToArray();
                var numArgs = args.Length;

                var closestMatch = command.Responses.FirstOrDefault(r => r.Inputs.Count == numArgs);
                if (closestMatch == null)
                {
                    closestMatch = command.Responses
                                    .OrderBy(r => r.Inputs.Count)
                                    .GetBeforeOrDefault(r => r.Inputs.Count > numArgs);

                    if (closestMatch == null)
                        closestMatch = command.Responses.Last();
                }

                var sender = (message.Author as SocketGuildUser)?.Nickname ?? message.Author.Username;
                var mappingDict = new Dictionary<string, object> { { "sender", sender } };
                for (var i = 0; i < closestMatch.Inputs.Count; i++)
                {
                    var input = closestMatch.Inputs.FirstOrDefault();
                    if (input == null)
                        continue;

                    var arg = args[i];
                    if (i == closestMatch.Inputs.Count - 1)
                        arg = string.Join(' ', args.Skip(i));

                    mappingDict[input] = arg;
                }

                if (!command.RandomResponses.IsEmpty())
                    mappingDict["randomResponse"] = command.RandomResponses.RandomElement();

                reply = RuntimeStringInterpolator.Interpolate(closestMatch.Output, mappingDict);
            }

            if (!string.IsNullOrWhiteSpace(command.ImageTag))
            {
                var argument = string.Join(' ', splitMessage.Skip(1));
                var dir = Images[command.ImageTag];
                var fileName = dir.FileNameToPath(argument) ?? dir.Next();
                using var file = File.OpenRead(fileName);
                await SendFile(message.Channel, fileName, file, reply ?? Path.GetFileNameWithoutExtension(fileName));
            }
            else if (!string.IsNullOrWhiteSpace(reply))
                await SendMessage(message.Channel, reply);
        }

        public Task SendFile(IMessageChannel channel, string fileName, Stream stream, string? reply = null)
        {
            Log($">>> [#{channel.Name}] <{fileName}> {reply ?? "<null>"}");
            fileName = Path.GetFileName(fileName);
            return channel.SendFileAsync(stream, fileName, reply);
        }

        public async Task<IUserMessage> SendMessage(IMessageChannel channel, string message)
        {
            Log($">>> [#{channel.Name}] {message}");
            return await channel.SendMessageAsync(message);
        }

        private async Task HandleAction(SocketUserMessage message, ICommand command, string[] splitMessage)
        {
            var method = PluginMethod.FromFullMethodName(command.Action);
            await method.Invoke(this, message, command, DiscordClient, Config, splitMessage);
        }

        private void LoadImageDirectory()
        {
            Log($"Loading image directories.");
            images.Clear();
            foreach (var dir in Directory.EnumerateDirectories(ResolvedFileRoot))
            {
                var tag = Path.GetFileName(dir);
                images[tag] = new ImageDirectory(ResolvedFileRoot, tag);
            }
        }

        private async Task LoadEmojiMap()
        {
            Log($"Loading emoji map.");
            EmojiMap.Clear();
            if (Config.EmojiMapUri == null)
            {
                Log("Emoji map uri not configured", LogLevel.Warning);
                return;
            }
            else
            {
                var map = await EmojiMapClient.GetAsync(Config.EmojiMapUri)!;
                if (!map.IsSuccessStatusCode)
                {
                    Log($"Emoji map not downloaded: {map.StatusCode}", LogLevel.Warning);
                    return;
                }
                var content = await map.Content.ReadAsStringAsync();
                try
                {
                    var obj = JsonDocument.Parse(content);
                    var emojiDefinitions = obj.RootElement.GetProperty("emojiDefinitions");
                    if (emojiDefinitions.ValueKind != JsonValueKind.Array)
                    {
                        Log($"Could not parse emojiDefinitions.", LogLevel.Warning);
                        return;
                    }

                    var index = 0;
                    foreach (var emoji in emojiDefinitions.EnumerateArray())
                    {
                        var key = emoji.GetProperty("surrogates");
                        var names = emoji.GetProperty("names");
                        if (key.ValueKind != JsonValueKind.String || names.ValueKind != JsonValueKind.Array || names.GetArrayLength() == 0)
                            Log($"Could not prase emoji definition at index {index}.", LogLevel.Warning);
                        else
                            EmojiMap[key.GetString()!] = names.EnumerateArray().First().GetString()!;
                    }
                }
                catch (Exception ex)
                {
                    Log($"Error reading emoji map: {ex}", LogLevel.Warning);
                }
            }
        }

        public static async Task RandomPicture(IMessage message, Bot bot)
        {
            var dir = bot.Images.RandomElement().Value;
            var fileName = dir.Next();
            using var file = File.OpenRead(fileName);
            await bot.SendFile(message.Channel, fileName, file, $"{dir.Tag}: {Path.GetFileNameWithoutExtension(fileName)}");
        }

        public Task SetGame(IMessage message, IDiscordClient discordClient)
            => (discordClient as DiscordSocketClient)?.SetGameAsync(message.Content.Split(' ').Skip(1).Join(" ")) ?? Task.CompletedTask;

        public static async Task DisplayHelp(IMessage message, IConfig config)
        {
            var sb = new StringBuilder("Hello, I am a bot. Here's a list of my commands:");
            sb.AppendLine();

            foreach (var configCommand in config.Commands)
            {
                sb.AppendLine($"{configCommand.Triggers.First()}: {configCommand.Description}");
                if (configCommand.Triggers.Count > 1)
                {
                    sb.Append("\tAlternative triggers: ");
                    foreach (var trigger in configCommand.Triggers.Skip(1))
                    {
                        sb.Append($"{trigger}, ");
                    }
                    sb.Length -= 2;
                    sb.AppendLine();
                }
            }

            await message.Channel.SendMessageAsync(sb.ToString());
        }

        private async Task NotifyMaintainer(string message)
        {
            Log(message);
            try
            {
                var channel = DiscordClient.GetGuild(Config.Maintainer.GuildId)?.GetTextChannel(Config.Maintainer.ChannelId);
                await (channel?.SendMessageAsync($"{DiscordClient.GetUser(Config.Maintainer.UserId)?.Mention} {message}") ?? Task.CompletedTask);
            }
            catch
            {
                Log("Couldn't notify maintainer.", LogLevel.Error);
            }
        }
    }
}
