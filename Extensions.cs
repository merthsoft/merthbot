﻿using Discord.WebSocket;
using MerthBot.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MerthBot
{
    internal static class Extensions
    {
        private static readonly Random staticRandom = new Random();

        public static bool IsNullOrEmpty(this string s)
            => string.IsNullOrEmpty(s);

        public static string Join<T>(this IEnumerable<T> set, string separator, int? count = null)
            => string.Join(separator, count == null ? set : set.Take(count.Value));

        public static string Join<T>(this IEnumerable<T> set, string separator, Func<T, object> selector)
            => string.Join(separator, set.Select(selector));

        public static bool IsEmpty<T>(this IEnumerable<T> set)
            => (!set?.Any()) ?? true;

        public static bool ContainsIgnoreCase(this IEnumerable<string> set, string value)
            => set.Any(s => s.Equals(value, StringComparison.InvariantCultureIgnoreCase));

        public static T? GetBeforeOrDefault<T>(this IEnumerable<T> set, Func<T, bool> predicate) where T : class
        {
            T? previousItem = default;
            foreach (var item in set)
            {
                if (predicate(item))
                    return previousItem;
            }

            return default;
        }

        public static T RandomElement<T>(this IEnumerable<T> set)
            => set.ElementAt(staticRandom.Next(set.Count()));

        public static (ICommand?, string[]) FindCommand(this SocketUserMessage message, IConfig config)
        {
            var messageText = message.Content;
            var splitMessage = messageText.Split(' ');
            var trigger = splitMessage[0];
            var command = config.Commands.FirstOrDefault(c => c.Triggers.ContainsIgnoreCase(trigger));
            return (command, splitMessage);
        }

        public static TResult[] SelectArray<TSource, TResult>(this IEnumerable<TSource> set, Func<TSource, TResult> selector)
        {
            var ret = new List<TResult>();
            foreach (var t in set)
                ret.Add(selector(t));

            return ret.ToArray();
        }
    }
}
