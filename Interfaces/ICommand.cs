﻿using System.Collections.Generic;

namespace MerthBot.Interfaces
{
    public interface ICommand
    {
        string Action { get; }
        string Description { get; }
        string ImageTag { get; }
        string Name { get; }
        IReadOnlyCollection<string> RandomResponses { get; }
        IReadOnlyCollection<IResponse> Responses { get; }
        IReadOnlyCollection<string> Triggers { get; }
        IReadOnlyDictionary<string, object> ExtraData { get; }
    }
}
