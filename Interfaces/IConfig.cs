﻿using System;
using System.Collections.Generic;

namespace MerthBot.Interfaces
{
    public interface IConfig
    {
        IReadOnlyCollection<ICommand> Commands { get; }
        Uri EmojiMapUri { get; }
        string? FileRoot { get; }
        string? FileRootEnvironmentVariable { get; }
        string? Game { get; }
        IMaintainer Maintainer { get; }
        int? MessageCacheSize { get; }
        string TokenEnvironmentVariable { get; }
        IReadOnlyDictionary<string, object> ExtraData { get; }
    }
}
