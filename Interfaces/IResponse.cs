﻿using System.Collections.Generic;

namespace MerthBot.Interfaces
{
    public interface IResponse
    {
        IReadOnlyCollection<string> Inputs { get; }
        string Output { get; }
    }
}
