﻿namespace MerthBot.Interfaces
{
    public interface IMaintainer
    {
        ulong ChannelId { get; }
        ulong GuildId { get; }
        ulong UserId { get; }
    }
}
