﻿using System;

namespace MerthBot
{
    internal class RequestException : Exception
    {
        public string? Body { get; }

        public bool HasBody
            => !string.IsNullOrWhiteSpace(Body);

        public RequestException(string message, string? body) : base(message)
            => Body = body;
    }
}
