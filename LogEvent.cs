﻿using System;
using System.Runtime.CompilerServices;

namespace MerthBot
{
    public class LogEvent : EventArgs
    {
        public string Message { get; }
        public LogLevel LogLevel { get; }

        public LogEvent(string message, LogLevel logLevel = LogLevel.Message)
            => (Message, LogLevel) = (message, logLevel);
    }
}
