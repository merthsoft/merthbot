﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MerthBot
{
    public class ImageDirectory
    {
        public string FileRoot { get; }

        public string Tag { get; }


        private string? LastImage { get; set; }

        private IEnumerable<string> Files { get; set; }

        private IEnumerator<string> ShuffledEntries { get; set; }

        // Disable warning, because Shuffle() initializes the fields;
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        public ImageDirectory(string fileRoot, string tag)
        {
            _ = tag ?? throw new ArgumentException($"'{nameof(tag)}' cannot be null or empty", nameof(tag));
            _ = fileRoot ?? throw new ArgumentException($"'{nameof(fileRoot)}' cannot be null or empty", nameof(fileRoot));

            (FileRoot, Tag) = (fileRoot, tag);
            Shuffle();
        }
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.

        public string? FileNameToPath(string image)
            => Files.FirstOrDefault(i => Path.GetFileNameWithoutExtension(i) == image);

        public void Shuffle()
        {
            Files = Directory.EnumerateFiles(Path.Combine(FileRoot, Tag));
            ShuffledEntries = Files.OrderBy(s => Guid.NewGuid()).GetEnumerator();
        }

        public string Next()
        {
            if (!ShuffledEntries.MoveNext())
            {
                Shuffle();
                ShuffledEntries.MoveNext();
            }
            LastImage = ShuffledEntries.Current;
            return ShuffledEntries.Current;
        }
    }
}
