﻿using System;
using System.Reflection;

namespace MerthBot.Exceptions
{
    public class MerthBotException : Exception
    {
        public MerthBotException(string message) : base(message) { }

        public MerthBotException(string message, Exception innerException) : base(message, innerException) { }
    }
}
