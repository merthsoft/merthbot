﻿using System;
using System.Reflection;

namespace MerthBot.Exceptions
{
    public class MerthBotPluginExecutionException : MerthBotException
    {
        public MethodInfo PluginMethod { get; }

        public MerthBotPluginExecutionException(MethodInfo pluginMethod, string message, Exception innerException) : base(message, innerException)
            => PluginMethod = pluginMethod;
    }
}
