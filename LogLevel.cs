﻿using System;
using System.Linq;

namespace MerthBot
{
    public enum LogLevel
    {
        Debug,
        Message,
        Warning,
        Error
    }
}
