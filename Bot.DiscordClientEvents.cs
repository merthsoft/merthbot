﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MerthBot
{
    partial class Bot
    {
        private async Task DiscordClient_Connected()
        {
            Log("Connected.");
            if (Config.Game != null)
                await DiscordClient.SetGameAsync(Config.Game, type: ActivityType.Streaming);
        }

        private Task DiscordClient_Disconnected(Exception? arg)
        {
            Log("Client disconnected.", LogLevel.Error);
            while (arg != null && !(arg is TaskCanceledException))
            {
                Log("----------", LogLevel.Error);
                Log(arg.ToString(), LogLevel.Debug);

                arg = arg.InnerException;
            }

            return Task.CompletedTask;
        }

        private Task DiscordClient_GuildAvailable(SocketGuild guild)
        {
            Log($"Guild available: {guild.Name}");
            if (!FirstStart || (guild.Id != Config.Maintainer.GuildId))
                return Task.CompletedTask;

            FirstStart = false;
            return NotifyMaintainer("I have risen.");
        }

        private Task DiscordClient_GuildUnavailable(SocketGuild guild)
        {
            Log($"Guild unavailable: {guild.Name}");
            return Task.CompletedTask;
        }

        private async Task DiscordClient_MessageDeleted(Cacheable<IMessage, ulong> messageCache, ISocketMessageChannel channel)
        {
            var message = await messageCache.GetOrDownloadAsync();
            if (message == null)
                Log($">>> A message so old it's not in the cache was deleted.");
            else
            {
                Log($">>> Message deleted:");
                Log($">>> [{message.Author.Username}] {message.Content}");
            }
        }

        private async Task DiscordClient_MessageReceived(SocketMessage rawMessage)
        {
            // Ignore system messages and messages from bots
            if (!(rawMessage is SocketUserMessage message))
                return;

            if (message.Source != MessageSource.User)
                return;

            Log($"<<< [#{message.Channel.Name}] {message.Author.Username}: {(message.Attachments.Any() ? "<" + message.Attachments.Join(", ", a => a.Url) + "> " : string.Empty)}{message.Content}");

            try
            {
                await HandleMessage(message);
            }
            catch (Exception ex)
            {
                Log($"Exception processing message {message.Content}: {ex}", LogLevel.Error);
            }
        }

        private Task DiscordClient_MessageUpdated(Cacheable<IMessage, ulong> messageCache, SocketMessage message, ISocketMessageChannel channel)
        {
            Log($">>> Message edited:");
            Log($">>> [{message.Author.Username}] {message.Content}");
            return Task.CompletedTask;
        }

        private async Task DiscordClient_ReactionAdded(Cacheable<IUserMessage, ulong> messageCache, ISocketMessageChannel channel, SocketReaction reaction)
        {
            var message = await messageCache.GetOrDownloadAsync();
            Log($">>> {reaction.User.Value.Username} added reaction :{EmojiMap.GetValueOrDefault(reaction.Emote.Name, reaction.Emote.Name)}: to message:");
            Log($">>> [{message.Author.Username}] {message.Content}");
        }

        private async Task DiscordClient_ReactionRemoved(Cacheable<IUserMessage, ulong> messageCache, ISocketMessageChannel channel, SocketReaction reaction)
        {
            var message = await messageCache.GetOrDownloadAsync();
            Log($">>> {reaction.User.Value.Username} removed their reaction :{EmojiMap.GetValueOrDefault(reaction.Emote.Name, reaction.Emote.Name)}: to message:");
            Log($">>> [{message.Author.Username}] {message.Content}");
        }

        private async Task DiscordClient_ReactionsCleared(Cacheable<IUserMessage, ulong> messageCache, ISocketMessageChannel channel)
        {
            var message = await messageCache.GetOrDownloadAsync();
            Log($">>> Reactions cleared on message:");
            Log($">>> [{message.Author.Username}] {message.Content}");
        }
    }
}
